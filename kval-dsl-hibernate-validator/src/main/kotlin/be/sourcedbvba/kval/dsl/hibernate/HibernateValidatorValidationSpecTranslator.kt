package be.sourcedbvba.kval.dsl.hibernate

import be.sourcedbvba.kval.dsl.*
import be.sourcedbvba.kval.dsl.hibernate.constraints.NotNullIfOtherIsNotNullDef
import be.sourcedbvba.kval.dsl.hibernate.constraints.NullIfOtherIsNotNullDef
import be.sourcedbvba.kval.dsl.hibernate.constraints.TemporalBeforeDef
import org.hibernate.validator.HibernateValidator
import org.hibernate.validator.HibernateValidatorConfiguration
import org.hibernate.validator.cfg.ConstraintDef
import org.hibernate.validator.cfg.ConstraintMapping
import org.hibernate.validator.cfg.defs.*
import org.hibernate.validator.internal.cfg.context.DefaultConstraintMapping
import org.hibernate.validator.messageinterpolation.ParameterMessageInterpolator
import java.lang.annotation.ElementType
import javax.validation.Validation
import javax.validation.Validator
import kotlin.reflect.KClass

class HibernateValidatorValidationSpecTranslator() : ValidationSpecTranslator<Validator> {
    private val constraints: MutableMap<KClass<out ConstraintRule<out Any?>>, ConstraintRuleTranslator<out ConstraintRule<out Any?>>>

    init {
        constraints = mutableMapOf()
        registerDefaultObjectConstraints()
        registerDefaultStringConstraints()
        registerDefaultNumberConstraints()
        registerDefaultTemporalConstraints()
    }

    private fun registerDefaultTemporalConstraints() {
        registerConstraint(TemporalFuture::class, ConstraintRuleTranslator { FutureDef() })
        registerConstraint(TemporalPast::class, ConstraintRuleTranslator { PastDef() })
        registerConstraint(TemporalBeforeProperty::class, ConstraintRuleTranslator { TemporalBeforeDef().propertyA(it.propertyA).propertyB(it.propertyB) })
    }

    private fun registerDefaultNumberConstraints() {
        registerConstraint(NaturalNumberMin::class, ConstraintRuleTranslator { MinDef().value(it.value.toLong()) })
        registerConstraint(NaturalNumberMax::class, ConstraintRuleTranslator { MaxDef().value(it.value.toLong()) })
        registerConstraint(DecimalNumberMin::class, ConstraintRuleTranslator { DecimalMinDef().value(it.value.toString()) })
        registerConstraint(DecimalNumberMax::class, ConstraintRuleTranslator { DecimalMaxDef().value(it.value.toString()) })
        registerConstraint(PositiveNumber::class, ConstraintRuleTranslator { PositiveDef() })
        registerConstraint(NegativeNumber::class, ConstraintRuleTranslator { NegativeDef() })
        registerConstraint(NaturalNumberRange::class, ConstraintRuleTranslator { RangeDef().min(it.min.toLong()).max(it.max.toLong()) })
    }

    private fun registerDefaultObjectConstraints() {
        registerConstraint(ObjectNotNull::class, ConstraintRuleTranslator { NotNullDef() })
        registerConstraint(ObjectNotNullIfOtherFieldNotNull::class, ConstraintRuleTranslator { NotNullIfOtherIsNotNullDef().propertyA(it.propertyA).propertyB(it.propertyB) })
        registerConstraint(ObjectNullIfOtherFieldNotNull::class, ConstraintRuleTranslator { NullIfOtherIsNotNullDef().propertyA(it.propertyA).propertyB(it.propertyB) })
        registerConstraint(ObjectNull::class, ConstraintRuleTranslator { NullDef() })

    }

    private fun registerDefaultStringConstraints() {
        registerConstraint(CharSequenceNotBlank::class, ConstraintRuleTranslator { NotBlankDef() })
        registerConstraint(CharSequenceSize::class, ConstraintRuleTranslator { SizeDef().min(it.min).max(it.max) })
        registerConstraint(ValidEmailAddress::class, ConstraintRuleTranslator { EmailDef() })
        registerConstraint(ValidURL::class, ConstraintRuleTranslator { URLDef() })
        registerConstraint(CompliesWithRegex::class, ConstraintRuleTranslator { PatternDef().regexp(it.pattern.toString()) })
        registerConstraint(ValidCreditCardNumber::class, ConstraintRuleTranslator { CreditCardNumberDef() })
    }

    fun <T : ConstraintRuleTranslator<out C>, C : ConstraintRule<out Any?>> registerConstraint(constraintRuleClass: KClass<out C>, translator: T) {
        constraints.put(constraintRuleClass, translator)
    }

    class ConstraintRuleTranslator<C : ConstraintRule<out Any?>>(private val block: (C) -> ConstraintDef<*, *>) {
        fun translate(c: C): ConstraintDef<*, *> = block.invoke(c)
    }

    private fun toConstraintMapping(spec: ValidationSpec): ConstraintMapping {
        return with(spec) {
            val constraintMapping = DefaultConstraintMapping()
            constraints.forEach {
                val typeMapping = constraintMapping.type(it.klass.java)
                it.fieldConstraints.forEach {
                    val propertyMapping = typeMapping.property(it.property.name, ElementType.FIELD)
                    it.constraintRules.forEach { rule: ConstraintRule<out Any?> ->
                        if(rule is MultiPropertyConstraintRule) {
                            val c = typeMapping.constraint(toConstraintDef(rule))
                        } else {
                            val c = propertyMapping.constraint(toConstraintDef(rule))
                        }
                    }
                }
            }
            constraintMapping
        }
    }

    private fun <R : ConstraintRule<out Any?>> toConstraintDef(rule: R): ConstraintDef<*, *> {
        if (constraints.containsKey(rule::class)) {
            val translator = constraints[rule::class] as ConstraintRuleTranslator<R>
            return translator.translate(rule)
        } else {
            throw IllegalStateException()
        }
    }

    override fun translate(validationSpec: ValidationSpec): Validator {
        val config = getConfiguration(validationSpec)
        val factory = config.buildValidatorFactory()
        return factory.validator
    }

    protected fun getConfiguration(validationSpec: ValidationSpec): HibernateValidatorConfiguration {
        val constraintMapping = toConstraintMapping(validationSpec)
        val config = Validation.byProvider(HibernateValidator::class.java).configure()
        config.addMapping(constraintMapping)
        return config
    }
}