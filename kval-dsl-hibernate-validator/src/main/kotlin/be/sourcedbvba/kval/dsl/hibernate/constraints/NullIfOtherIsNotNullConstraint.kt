package be.sourcedbvba.kval.dsl.hibernate.constraints

import org.hibernate.validator.cfg.ConstraintDef
import java.time.chrono.ChronoLocalDate
import java.time.chrono.ChronoLocalDateTime
import java.time.temporal.Temporal
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

@Constraint(validatedBy = [(NullIfOtherIsNotNullValidator::class)])
annotation class NullIfOtherIsNotNull(val propertyA: String,
                                val propertyB: String,
                                val message: String = "{constraints.object.notNullIfOtherNull}",
                                val groups: Array<KClass<*>> = arrayOf(),
                                val payload : Array<KClass<out Payload>> = arrayOf())

class NullIfOtherIsNotNullDef : ConstraintDef<NullIfOtherIsNotNullDef, NullIfOtherIsNotNull>(NullIfOtherIsNotNull::class.java) {
    fun propertyA(property: KProperty1<*, *>): NullIfOtherIsNotNullDef {
        addParameter("propertyA", property.name)
        return this
    }

    fun propertyB(property: KProperty1<*, *>): NullIfOtherIsNotNullDef {
        addParameter("propertyB", property.name)
        return this
    }
}

class NullIfOtherIsNotNullValidator : ConstraintValidator<NullIfOtherIsNotNull, Any> {

    private lateinit var constraintAnnotation: NullIfOtherIsNotNull

    override fun isValid(value: Any, context: ConstraintValidatorContext?): Boolean {
        val propertyAName = constraintAnnotation.propertyA
        val propertyBName = constraintAnnotation.propertyB
        val propertyA = value::class.members.first { it.name == propertyAName }.call(value)
        val propertyB = value::class.members.first { it.name == propertyBName }.call(value)
        if(propertyB != null) {
            return propertyA == null
        } else {
            return true
        }
    }

    override fun initialize(constraintAnnotation: NullIfOtherIsNotNull) {
        this.constraintAnnotation = constraintAnnotation
    }
}