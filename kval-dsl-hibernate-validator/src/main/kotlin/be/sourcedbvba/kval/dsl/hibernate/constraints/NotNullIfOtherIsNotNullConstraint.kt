package be.sourcedbvba.kval.dsl.hibernate.constraints

import org.hibernate.validator.cfg.ConstraintDef
import java.time.chrono.ChronoLocalDate
import java.time.chrono.ChronoLocalDateTime
import java.time.temporal.Temporal
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

@Constraint(validatedBy = [(NotNullIfOtherIsNotNullValidator::class)])
annotation class NotNullIfOtherIsNotNull(val propertyA: String,
                                val propertyB: String,
                                val message: String = "{constraints.object.notNullIfOtherNotNull}",
                                val groups: Array<KClass<*>> = arrayOf(),
                                val payload : Array<KClass<out Payload>> = arrayOf())

class NotNullIfOtherIsNotNullDef : ConstraintDef<NotNullIfOtherIsNotNullDef, NotNullIfOtherIsNotNull>(NotNullIfOtherIsNotNull::class.java) {
    fun propertyA(property: KProperty1<*, *>): NotNullIfOtherIsNotNullDef {
        addParameter("propertyA", property.name)
        return this
    }

    fun propertyB(property: KProperty1<*, *>): NotNullIfOtherIsNotNullDef {
        addParameter("propertyB", property.name)
        return this
    }
}

class NotNullIfOtherIsNotNullValidator : ConstraintValidator<NotNullIfOtherIsNotNull, Any> {

    private lateinit var constraintAnnotation: NotNullIfOtherIsNotNull

    override fun isValid(value: Any, context: ConstraintValidatorContext?): Boolean {
        val propertyAName = constraintAnnotation.propertyA
        val propertyBName = constraintAnnotation.propertyB
        val propertyA = value::class.members.first { it.name == propertyAName }.call(value)
        val propertyB = value::class.members.first { it.name == propertyBName }.call(value)
        if(propertyB != null) {
            return propertyA != null
        } else {
            return true
        }
    }

    override fun initialize(constraintAnnotation: NotNullIfOtherIsNotNull) {
        this.constraintAnnotation = constraintAnnotation
    }
}