package be.sourcedbvba.kval.dsl.hibernate.constraints

import org.hibernate.validator.cfg.ConstraintDef
import java.time.chrono.ChronoLocalDate
import java.time.chrono.ChronoLocalDateTime
import java.time.temporal.Temporal
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

@Constraint(validatedBy = [(TemporalBeforeValidator::class)])
annotation class TemporalABeforeB(val propertyA: String,
                                val propertyB: String,
                                val message: String = "{constraints.temporal.aBeforeB}",
                                val groups: Array<KClass<*>> = arrayOf(),
                                val payload : Array<KClass<out Payload>> = arrayOf())

class TemporalBeforeDef : ConstraintDef<TemporalBeforeDef, TemporalABeforeB>(TemporalABeforeB::class.java) {
    fun propertyA(property: KProperty1<*, *>): TemporalBeforeDef {
        addParameter("propertyA", property.name)
        return this
    }

    fun propertyB(property: KProperty1<*, *>): TemporalBeforeDef {
        addParameter("propertyB", property.name)
        return this
    }
}

class TemporalBeforeValidator : ConstraintValidator<TemporalABeforeB, Any> {

    private lateinit var constraintAnnotation: TemporalABeforeB

    override fun isValid(value: Any, context: ConstraintValidatorContext?): Boolean {
        val propertyAName = constraintAnnotation.propertyA
        val propertyBName = constraintAnnotation.propertyB
        val propertyA = value::class.members.first { it.name == propertyAName }.call(value) as Temporal
        val propertyB = value::class.members.first { it.name == propertyBName }.call(value) as Temporal
        return propertyA.isBeforeTemporal(propertyB)
    }


    override fun initialize(constraintAnnotation: TemporalABeforeB) {
        this.constraintAnnotation = constraintAnnotation
    }
}

inline fun <reified T : Temporal> T.isBeforeTemporal(temporal : T) : Boolean {
    return when {
        this is ChronoLocalDate -> this.isBefore(temporal as ChronoLocalDate)
        this is ChronoLocalDateTime<*> -> this.isBefore(temporal as ChronoLocalDateTime<*>)
        else -> throw IllegalArgumentException("Can only use before on LocalDate and LocalDateTime fields")
    }
}