package be.sourcedbvba.kval.dsl.hibernate

import be.sourcedbvba.kval.dsl.*
import io.damo.aspen.Test
import org.assertj.core.api.Assertions.assertThat
import javax.validation.Validator

class HibernateValidatorValidationSpecTranslatorStringTest : Test({
    describe("not null constraint") {
        class Simple(val stringValue: String?)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::stringValue) {
                    notNull()
                }
            }
        }
        val validator = translator.translate(spec)

        test("with null") {
            val constraintViolations = validator.validate(Simple(null))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with non null") {
            val constraintViolations = validator.validate(Simple("abc"))
            assertThat(constraintViolations).hasSize(0)
        }
    }

    describe("not blank constraint") {
        class Simple(val stringValue: String)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::stringValue) {
                    notBlank()
                }
            }
        }
        val validator = translator.translate(spec)

        test("with blank string") {
            val constraintViolations = validator.validate(Simple("   "))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with non blank string") {
            val constraintViolations = validator.validate(Simple("abc"))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with empty string") {
            val constraintViolations = validator.validate(Simple(""))
            assertThat(constraintViolations).hasSize(1)
        }
    }

    describe("size constraint") {
        class Simple(val stringValue: String)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::stringValue) {
                    size(min = 5, max = 10)
                }
            }
        }
        val validator = translator.translate(spec)

        test("too short string") {
            val constraintViolations = validator.validate(Simple("abc"))
            assertThat(constraintViolations).hasSize(1)
        }

        test("too long string") {
            val constraintViolations = validator.validate(Simple("abcdefghijkl"))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with correct string") {
            val constraintViolations = validator.validate(Simple("abcdef"))
            assertThat(constraintViolations).hasSize(0)
        }
    }

    describe("email constraint") {
        class Simple(val stringValue: String)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::stringValue) {
                    emailAddress()
                }
            }
        }
        val validator = translator.translate(spec)

        test("with valid email") {
            val constraintViolations = validator.validate(Simple("test@example.com"))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with invalid email") {
            val constraintViolations = validator.validate(Simple("testab"))
            assertThat(constraintViolations).hasSize(1)
        }
    }

    describe("url constraint") {
        class Simple(val stringValue: String)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::stringValue) {
                    urlAddress()
                }
            }
        }
        val validator = translator.translate(spec)

        test("with valid url") {
            val constraintViolations = validator.validate(Simple("http://www.google.com"))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with invalid url") {
            val constraintViolations = validator.validate(Simple("blah"))
            assertThat(constraintViolations).hasSize(1)
        }
    }

    describe("regex constraint") {
        class Simple(val stringValue: String)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::stringValue) {
                    regex("[a-zA-Z]{3}")
                }
            }
        }
        val validator = translator.translate(spec)

        test("with valid value") {
            val constraintViolations = validator.validate(Simple("AbC"))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with invalid value") {
            val constraintViolations = validator.validate(Simple("AbCDe"))
            assertThat(constraintViolations).hasSize(1)
        }
    }

    describe("credit card constraint") {
        class Simple(val stringValue: String)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::stringValue) {
                    creditCardNumber()
                }
            }
        }
        val validator = translator.translate(spec)

        test("with valid value") {
            val constraintViolations = validator.validate(Simple("378145788679824"))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with invalid value") {
            val constraintViolations = validator.validate(Simple("3781457886798244"))
            assertThat(constraintViolations).hasSize(1)
        }
    }
})