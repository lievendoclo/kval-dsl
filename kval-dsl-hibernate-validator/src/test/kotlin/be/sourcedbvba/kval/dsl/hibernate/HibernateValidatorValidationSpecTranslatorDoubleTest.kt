package be.sourcedbvba.kval.dsl.hibernate

import be.sourcedbvba.kval.dsl.*
import io.damo.aspen.Test
import org.assertj.core.api.Assertions.assertThat
import javax.validation.Validator

class HibernateValidatorValidationSpecTranslatorDoubleTest : Test({
    describe("min constraint") {
        class Simple(val doubleValue: Double)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::doubleValue) {
                    min(5.0)
                }
            }
        }
        val validator = translator.translate(spec)

        test("with too low value") {
            val constraintViolations = validator.validate(Simple(4.9))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with correct value") {
            val constraintViolations = validator.validate(Simple(5.1))
            assertThat(constraintViolations).hasSize(0)
        }
    }

    describe("max constraint") {
        class Simple(val doubleValue: Double)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::doubleValue) {
                    max(5.0)
                }
            }
        }
        val validator = translator.translate(spec)

        test("with too high value") {
            val constraintViolations = validator.validate(Simple(5.1))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with correct value") {
            val constraintViolations = validator.validate(Simple(4.9))
            assertThat(constraintViolations).hasSize(0)
        }
    }

    describe("positive constraint") {
        class Simple(val value: Double)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::value) {
                    positive()
                }
            }
        }
        val validator = translator.translate(spec)

        test("with negative value") {
            val constraintViolations = validator.validate(Simple(-6.0))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with positive value") {
            val constraintViolations = validator.validate(Simple(4.0))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with zero value") {
            val constraintViolations = validator.validate(Simple(0.0))
            assertThat(constraintViolations).hasSize(1)
        }
    }

    describe("negative constraint") {
        class Simple(val value: Double)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::value) {
                    negative()
                }
            }
        }
        val validator = translator.translate(spec)

        test("with negative value") {
            val constraintViolations = validator.validate(Simple(-6.0))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with positive value") {
            val constraintViolations = validator.validate(Simple(4.0))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with zero value") {
            val constraintViolations = validator.validate(Simple(0.0))
            assertThat(constraintViolations).hasSize(1)
        }
    }
})