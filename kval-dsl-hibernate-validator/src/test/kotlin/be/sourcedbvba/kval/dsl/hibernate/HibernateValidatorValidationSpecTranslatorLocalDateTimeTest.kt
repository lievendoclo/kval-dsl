package be.sourcedbvba.kval.dsl.hibernate

import be.sourcedbvba.kval.dsl.*
import io.damo.aspen.Test
import org.assertj.core.api.Assertions.assertThat
import java.time.LocalDate
import java.time.LocalDateTime
import javax.validation.Validator

class HibernateValidatorValidationSpecTranslatorLocalDateTimeTest : Test({
    describe("future constraint") {
        class Simple(val temporalValue: LocalDateTime)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::temporalValue) {
                    inTheFuture()
                }
            }
        }
        val validator = translator.translate(spec)

        test("with past date") {
            val constraintViolations = validator.validate(Simple(LocalDateTime.now().minusSeconds(1)))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with future date") {
            val constraintViolations = validator.validate(Simple(LocalDateTime.now().plusSeconds(1)))
            assertThat(constraintViolations).hasSize(0)
        }
    }

    describe("past constraint") {
        class Simple(val temporalValue: LocalDateTime)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::temporalValue) {
                    inThePast()
                }
            }
        }
        val validator = translator.translate(spec)

        test("with past date") {
            val constraintViolations = validator.validate(Simple(LocalDateTime.now().minusSeconds(1)))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with future date") {
            val constraintViolations = validator.validate(Simple(LocalDateTime.now().plusSeconds(1)))
            assertThat(constraintViolations).hasSize(1)
        }
    }

    describe("before constraint") {
        class Simple(val temporalValueStart: LocalDateTime, val temporalValueStop: LocalDateTime)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::temporalValueStart) {
                    before(Simple::temporalValueStop)
                }
            }
        }
        val validator = translator.translate(spec)

        test("with start after stop date") {
            val constraintViolations = validator.validate(Simple(LocalDateTime.now(), LocalDateTime.now().minusSeconds(1)))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with start before stop date") {
            val constraintViolations = validator.validate(Simple(LocalDateTime.now(), LocalDateTime.now().plusSeconds(1)))
            assertThat(constraintViolations).hasSize(0)
        }
    }
})