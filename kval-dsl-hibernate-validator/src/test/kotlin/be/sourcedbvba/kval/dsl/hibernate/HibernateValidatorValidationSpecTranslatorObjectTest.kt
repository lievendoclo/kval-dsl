package be.sourcedbvba.kval.dsl.hibernate

import be.sourcedbvba.kval.dsl.*
import io.damo.aspen.Test
import org.assertj.core.api.Assertions.assertThat
import javax.validation.Validator

class HibernateValidatorValidationSpecTranslatorObjectTest : Test({
    describe("not null constraint") {
        class Simple(val doubleValue: Int?)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::doubleValue) {
                    notNull()
                }
            }
        }
        val validator = translator.translate(spec)

        test("with null value") {
            val constraintViolations = validator.validate(Simple(null))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with non-null value") {
            val constraintViolations = validator.validate(Simple(5))
            assertThat(constraintViolations).hasSize(0)
        }
    }

    describe("null constraint") {
        class Simple(val doubleValue: Int?)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::doubleValue) {
                    shouldBeNull()
                }
            }
        }
        val validator = translator.translate(spec)

        test("with null value") {
            val constraintViolations = validator.validate(Simple(null))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with non-null value") {
            val constraintViolations = validator.validate(Simple(5))
            assertThat(constraintViolations).hasSize(1)
        }
    }

    describe("conditional null constraint") {
        class Simple(val valueA: Int?, val valueB: Int?)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::valueA) {
                   shouldBeNullIfOtherFieldIsNotNull(Simple::valueB)
                }
            }
        }
        val validator = translator.translate(spec)

        test("with A null and B null") {
            val constraintViolations = validator.validate(Simple(null, null))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with A null and B not null") {
            val constraintViolations = validator.validate(Simple(null, 1))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with A not null and B not null") {
            val constraintViolations = validator.validate(Simple(1, 1))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with A not null and B null") {
            val constraintViolations = validator.validate(Simple(1, null))
            assertThat(constraintViolations).hasSize(0)
        }

    }

    describe("conditional not null constraint") {
        class Simple(val valueA: Int?, val valueB: Int?)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::valueA) {
                    notNullIfOtherFieldIsNotNull(Simple::valueB)
                }
            }
        }
        val validator = translator.translate(spec)

        test("with A null and B null") {
            val constraintViolations = validator.validate(Simple(null, null))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with A null and B not null") {
            val constraintViolations = validator.validate(Simple(null, 1))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with A not null and B not null") {
            val constraintViolations = validator.validate(Simple(1, 1))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with A not null and B null") {
            val constraintViolations = validator.validate(Simple(1, null))
            assertThat(constraintViolations).hasSize(0)
        }

    }
})