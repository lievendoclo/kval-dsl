package be.sourcedbvba.kval.dsl.hibernate

import be.sourcedbvba.kval.dsl.*
import io.damo.aspen.Test
import org.assertj.core.api.Assertions.*
import org.hibernate.validator.cfg.defs.NotNullDef
import java.lang.IllegalArgumentException
import javax.validation.Validator

object UnmappedConstraint : ConstraintRule<String>

fun FieldConstraint<out Any, String>.unmapped() = constraintRules.add(UnmappedConstraint)

class HibernateValidatorValidationSpecTranslatorUnmappedConstraintTest : Test({
    describe("unmapped constraint") {
        class Simple(val value: String)

        test("should fail") {
            assertThatThrownBy {
                val spec = validationSpec {
                    constraints<Simple> {
                        field(Simple::value) {
                            unmapped()
                        }
                    }
                }
                val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
                translator.translate(spec)
            }.isInstanceOf(IllegalStateException::class.java)
        }

        test("should work if added") {
            assertThatCode {
                val spec = validationSpec {
                    constraints<Simple> {
                        field(Simple::value) {
                            unmapped()
                        }
                    }
                }
                val translator = HibernateValidatorValidationSpecTranslator()
                translator.registerConstraint(UnmappedConstraint::class, HibernateValidatorValidationSpecTranslator.ConstraintRuleTranslator { NotNullDef()})
                translator.translate(spec)
            }.doesNotThrowAnyException()
        }
    }
})