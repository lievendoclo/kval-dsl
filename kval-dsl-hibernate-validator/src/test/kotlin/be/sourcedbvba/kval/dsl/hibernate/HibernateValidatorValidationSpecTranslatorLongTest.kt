package be.sourcedbvba.kval.dsl.hibernate

import be.sourcedbvba.kval.dsl.*
import io.damo.aspen.Test
import org.assertj.core.api.Assertions.assertThat
import javax.validation.Validator

class HibernateValidatorValidationSpecTranslatorLongTest : Test({
    describe("min constraint") {
        class Simple(val longValue: Long)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::longValue) {
                    min(5)
                }
            }
        }
        val validator = translator.translate(spec)

        test("with too low value") {
            val constraintViolations = validator.validate(Simple(4))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with correct value") {
            val constraintViolations = validator.validate(Simple(6))
            assertThat(constraintViolations).hasSize(0)
        }
    }

    describe("max constraint") {
        class Simple(val longValue: Long)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::longValue) {
                    max(5)
                }
            }
        }
        val validator = translator.translate(spec)

        test("with too high value") {
            val constraintViolations = validator.validate(Simple(6))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with correct value") {
            val constraintViolations = validator.validate(Simple(4))
            assertThat(constraintViolations).hasSize(0)
        }
    }

    describe("positive constraint") {
        class Simple(val value: Long)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::value) {
                    positive()
                }
            }
        }
        val validator = translator.translate(spec)

        test("with negative value") {
            val constraintViolations = validator.validate(Simple(-6))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with positive value") {
            val constraintViolations = validator.validate(Simple(4))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with zero value") {
            val constraintViolations = validator.validate(Simple(0))
            assertThat(constraintViolations).hasSize(1)
        }
    }

    describe("negative constraint") {
        class Simple(val value: Long)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::value) {
                    negative()
                }
            }
        }
        val validator = translator.translate(spec)

        test("with negative value") {
            val constraintViolations = validator.validate(Simple(-6))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with positive value") {
            val constraintViolations = validator.validate(Simple(4))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with zero value") {
            val constraintViolations = validator.validate(Simple(0))
            assertThat(constraintViolations).hasSize(1)
        }
    }

    describe("range constraint") {
        class Simple(val value: Long)

        val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
        val spec = validationSpec {
            constraints<Simple> {
                field(Simple::value) {
                    range(0, 10)
                }
            }
        }
        val validator = translator.translate(spec)

        test("with incorrect value") {
            val constraintViolations = validator.validate(Simple(-6))
            assertThat(constraintViolations).hasSize(1)
        }

        test("with correct value") {
            val constraintViolations = validator.validate(Simple(4))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with lower boundary value") {
            val constraintViolations = validator.validate(Simple(0))
            assertThat(constraintViolations).hasSize(0)
        }

        test("with upper boundary value") {
            val constraintViolations = validator.validate(Simple(10))
            assertThat(constraintViolations).hasSize(0)
        }
    }
})