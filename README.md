# KVal: a Kotlin validation DSL

## What is it?
KVal provides a framework agnostic approach to validation. For example, given a class Product with a name.

```kotlin
data class Product(val name: String)
```

If we want to validate whether the name is not blank, we can
write the following specification.

```kotlin
validationSpec {
    constraints<Producct> {
        field(Product::name) {
           notBlank()
        }
    }
}
```

You can then use a `ValidationSpecTranslator` implementation (currently there is a Hibernate Validator implementation) to
get a framework validator and validate the object against the specification.

```kotlin
val translator: ValidationSpecTranslator<Validator> = HibernateValidatorValidationSpecTranslator()
val spec = ...
val validator = translator.translate(spec)
val constraintViolations = validator.validate(...)
```

## Why use a DSL?

Sometimes you don't want to have your datastructures cluttered with validation logic
and/or annotations. Or you may not want those annotation libraries as a dependency
in your module. This DSL allows you to describe in an agnostic manner how
you want your object to be validated. 

This DSL is also compatible with the concept of Clean Architecture. The declarative 
DSL doesn't pull in any third party dependency (besides Kotlin offcourse). This means
you can use this validation in your application layer or domain layer without having to 
worry about having pulled in a technical framework into a module that shouldn't have any
technical dependencies at all. The usage of the translator can be deferred into
on of the infrastructure layers.

## Benefits of a DSL

When using Validation API annotations, this is perfectly valid at runtime:

```kotlin
data class Product(@Past(1) val name: String)
```

`Past` is an annotation only reserved for temporal fields, like dates. However, the annotation
system in Java cannot determine the validity of a annotation target based on its type. With the DSL
you cannot do this.

```kotlin
validationSpec {
    constraints<Producct> {
        field(Product::name) {
           inThePast()
        }
    }
}
```

This will fail at compile time as the `inThePast()` constraint is only applicable to fields that extend `Temporal`, which String doesn't.

This also has the added benefit of code completion: the list of available constraints is filtered based on the type of the field.

## Default supported validations

### Nullable values

* `notNull()`
* `shouldBeNull()`
* `notNullIfOtherIsNotNull(otherField)`
* `shouldBeNullIfOtherIsNotNull(otherField)`

### Numeric values (Int, Long, Float, Double, BigDecimal)

* `positive()`
* `negative()`
* `min(value)`
* `max(value)`
* `range(min, max)` (only for Int and Long for the moment)

### Date values

* `inThePast()`
* `inTheFuture()`
* `before(otherField)` (i.e. `before(Product::bestBeforeDate)`, painful to do with the Validation API, impossible without resorting to referencing fields as Strings)
* `after(otherField)` (i.e. `after(Product::expiryDate)`, painful to do with the Validation API, impossible without resorting to referencing fields as Strings)

### Character sequence values (String)

* `notBlank()`
* `validEmail()`
* `validURL()`
* `regex(pattern)`
* `creditCardNumber()`
* `size(min, max)` (minimum is default 0, maximum is default MAX_INT)