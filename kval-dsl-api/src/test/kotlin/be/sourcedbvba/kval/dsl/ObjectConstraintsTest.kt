package be.sourcedbvba.kval.dsl

import io.damo.aspen.Test
import org.assertj.core.api.Assertions.assertThat

class ObjectConstraintsTest : Test({
    describe("not null") {
        test {
            class Simple(val value: Any?)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::value) {
                        notNull()
                    }
                }
            }

            assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                assertThat(fieldConstraints).hasSize(1)
                assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::value }
                with(fieldConstraints[0]) {
                    assertThat(constraintRules).hasSize(1)
                    assertThat(constraintRules[0]).isEqualTo(ObjectNotNull)
                }
            }
        }
    }

    describe("null") {
        test {
            class Simple(val value: Any?)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::value) {
                        shouldBeNull()
                    }
                }
            }

            assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                assertThat(fieldConstraints).hasSize(1)
                assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::value }
                with(fieldConstraints[0]) {
                    assertThat(constraintRules).hasSize(1)
                    assertThat(constraintRules[0]).isEqualTo(ObjectNull)
                }
            }
        }
    }

    describe("not null if other is not null") {
        test {
            class Simple(val value: Any?, val other: Any?)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::value) {
                        notNullIfOtherFieldIsNotNull(Simple::other)
                    }
                }
            }

            assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                assertThat(fieldConstraints).hasSize(1)
                assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::value }
                with(fieldConstraints[0]) {
                    assertThat(constraintRules).hasSize(1)
                    assertThat(constraintRules[0]).isEqualTo(ObjectNotNullIfOtherFieldNotNull(Simple::value, Simple::other))
                }
            }
        }
    }

    describe("null if other is not null") {
        test {
            class Simple(val value: Any?, val other: Any?)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::value) {
                        shouldBeNullIfOtherFieldIsNotNull(Simple::other)
                    }
                }
            }

            assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                assertThat(fieldConstraints).hasSize(1)
                assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::value }
                with(fieldConstraints[0]) {
                    assertThat(constraintRules).hasSize(1)
                    assertThat(constraintRules[0]).isEqualTo(ObjectNullIfOtherFieldNotNull(Simple::value, Simple::other))
                }
            }
        }
    }
})