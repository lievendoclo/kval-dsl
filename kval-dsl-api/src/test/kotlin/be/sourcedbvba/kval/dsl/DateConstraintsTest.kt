package be.sourcedbvba.kval.dsl

import io.damo.aspen.Test
import org.assertj.core.api.Assertions.assertThat
import java.time.LocalDate
import java.time.LocalDateTime


class DateConstraintsTest : Test({

    describe("in the future") {
        test("local date") {
            class Simple(val temporalValue: LocalDate)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::temporalValue) {
                        inTheFuture()
                    }
                }
            }

            assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                assertThat(fieldConstraints).hasSize(1)
                assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::temporalValue }
                with(fieldConstraints[0]) {
                    assertThat(constraintRules).hasSize(1)
                    assertThat(constraintRules[0]).isEqualTo(TemporalFuture)
                }
            }
        }

        test("local datetime") {
            class Simple(val temporalValue: LocalDateTime)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::temporalValue) {
                        inTheFuture()
                    }
                }
            }

            assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                assertThat(fieldConstraints).hasSize(1)
                assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::temporalValue }
                with(fieldConstraints[0]) {
                    assertThat(constraintRules).hasSize(1)
                    assertThat(constraintRules[0]).isEqualTo(TemporalFuture)
                }
            }
        }
    }

    describe("before") {
        test("local date") {
            class Simple(val temporalValueStart: LocalDate, val temporalValueStop: LocalDate)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::temporalValueStart) {
                        before(Simple::temporalValueStop)
                    }
                }
            }

            assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                assertThat(fieldConstraints).hasSize(1)
                assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::temporalValueStart }
                with(fieldConstraints[0]) {
                    assertThat(constraintRules).hasSize(1)
                    assertThat(constraintRules[0]).isEqualTo(TemporalBeforeProperty(Simple::temporalValueStart, Simple::temporalValueStop))
                }
            }
        }
    }

    describe("after") {
        test("local date") {
            class Simple(val temporalValueStart: LocalDate, val temporalValueStop: LocalDate)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::temporalValueStop) {
                        after(Simple::temporalValueStart)
                    }
                }
            }

            assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                assertThat(fieldConstraints).hasSize(1)
                assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::temporalValueStop }
                with(fieldConstraints[0]) {
                    assertThat(constraintRules).hasSize(1)
                    assertThat(constraintRules[0]).isEqualTo(TemporalAfterProperty(Simple::temporalValueStop, Simple::temporalValueStart))
                }
            }
        }
    }
})