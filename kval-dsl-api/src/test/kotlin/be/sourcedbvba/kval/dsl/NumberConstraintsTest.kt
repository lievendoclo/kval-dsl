package be.sourcedbvba.kval.dsl

import io.damo.aspen.Test
import org.assertj.core.api.Assertions.assertThat

class NumberConstraintsTest: Test({

    describe("#minConstraint") {
        test("test Int") {
            class Simple(val intValue: Int)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::intValue) {
                        min(10)
                    }
                }
            }

            assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                assertThat(fieldConstraints).hasSize(1)
                assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::intValue }
                with(fieldConstraints[0]) {
                    assertThat(constraintRules).hasSize(1)
                    assertThat(constraintRules[0]).isEqualTo(NaturalNumberMin(10))
                }
            }
        }

        test("test Long") {
            class Simple(val longValue: Long)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::longValue) {
                        min(10)
                    }
                }
            }

            assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                assertThat(fieldConstraints).hasSize(1)
                assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::longValue }
                with(fieldConstraints[0]) {
                    assertThat(constraintRules).hasSize(1)
                    assertThat(constraintRules[0]).isEqualTo(NaturalNumberMin(10L))
                }
            }
        }
    }

    describe("#maxConstraint") {
        test("test Int") {
            class Simple(val intValue: Int)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::intValue) {
                        max(10)
                    }
                }
            }

            assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                assertThat(fieldConstraints).hasSize(1)
                assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::intValue }
                with(fieldConstraints[0]) {
                    assertThat(constraintRules).hasSize(1)
                    assertThat(constraintRules[0]).isEqualTo(NaturalNumberMax(10))
                }
            }
        }

        test("test Long") {
            class Simple(val longValue: Long)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::longValue) {
                        max(10)
                    }
                }
            }

            assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                assertThat(fieldConstraints).hasSize(1)
                assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::longValue }
                with(fieldConstraints[0]) {
                    assertThat(constraintRules).hasSize(1)
                    assertThat(constraintRules[0]).isEqualTo(NaturalNumberMax(10L))
                }
            }
        }
    }
})