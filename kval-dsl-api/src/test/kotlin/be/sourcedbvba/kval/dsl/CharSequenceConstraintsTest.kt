package be.sourcedbvba.kval.dsl

import io.damo.aspen.Test
import org.assertj.core.api.Assertions

class CharSequenceConstraintsTest : Test({
    describe("notBlank") {
        test {
            class Simple(val stringValue: String)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::stringValue) {
                        notBlank()
                    }
                }
            }

            Assertions.assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            Assertions.assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                Assertions.assertThat(fieldConstraints).hasSize(1)
                Assertions.assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::stringValue }
                with(fieldConstraints[0]) {
                    Assertions.assertThat(constraintRules).hasSize(1)
                    Assertions.assertThat(constraintRules[0]).isEqualTo(CharSequenceNotBlank)
                }
            }
        }
    }

    describe("size") {
        test {
            class Simple(val stringValue: String)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::stringValue) {
                        size(min = 10, max = 100)
                    }
                }
            }

            Assertions.assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            Assertions.assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                Assertions.assertThat(fieldConstraints).hasSize(1)
                Assertions.assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::stringValue }
                with(fieldConstraints[0]) {
                    Assertions.assertThat(constraintRules).hasSize(1)
                    Assertions.assertThat(constraintRules[0]).isEqualTo(CharSequenceSize(10, 100))
                }
            }
        }
    }

    describe("email") {
        test {
            class Simple(val stringValue: String)

            val spec = validationSpec {
                constraints<Simple> {
                    field(Simple::stringValue) {
                        emailAddress()
                    }
                }
            }

            Assertions.assertThat(spec.constraints).hasOnlyOneElementSatisfying { it.klass == Simple::class }
            Assertions.assertThat(spec.constraints).hasSize(1)
            with(spec.constraints[0]) {
                Assertions.assertThat(fieldConstraints).hasSize(1)
                Assertions.assertThat(fieldConstraints).hasOnlyOneElementSatisfying { it.property == Simple::stringValue }
                with(fieldConstraints[0]) {
                    Assertions.assertThat(constraintRules).hasSize(1)
                    Assertions.assertThat(constraintRules[0]).isEqualTo(ValidEmailAddress)
                }
            }
        }
    }
})