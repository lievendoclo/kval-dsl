package be.sourcedbvba.kval.dsl

import java.time.temporal.Temporal
import kotlin.reflect.KProperty1

object TemporalFuture : ConstraintRule<Temporal>
object TemporalPast : ConstraintRule<Temporal>
data class TemporalBeforeProperty<O : Any, T : Temporal>(val propertyA: KProperty1<O, T>, val propertyB: KProperty1<O, T>): MultiPropertyConstraintRule<T>
data class TemporalAfterProperty<O : Any, T : Temporal>(val propertyA: KProperty1<O, T>, val propertyB: KProperty1<O, T>): MultiPropertyConstraintRule<T>

fun <T : Temporal> FieldConstraint<out Any, T>.inTheFuture() {
    constraintRules.add(TemporalFuture)
}

fun <T : Temporal> FieldConstraint<out Any, T>.inThePast() {
    constraintRules.add(TemporalPast)
}

fun <O: Any, T : Temporal> FieldConstraint<O, T>.before(property: KProperty1<O, T>) {
    constraintRules.add(TemporalBeforeProperty(this.property, property))
}

fun <O: Any, T : Temporal> FieldConstraint<O, T>.after(property: KProperty1<O, T>) {
    constraintRules.add(TemporalAfterProperty(this.property, property))
}
