package be.sourcedbvba.kval.dsl

interface ValidationSpecTranslator<T : Any> {
    fun translate(validationSpec: ValidationSpec) : T
}