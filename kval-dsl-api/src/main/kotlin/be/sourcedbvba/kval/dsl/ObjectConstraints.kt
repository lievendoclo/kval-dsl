package be.sourcedbvba.kval.dsl

import kotlin.reflect.KProperty1

object ObjectNotNull : ConstraintRule<Any?>
data class  ObjectNotNullIfOtherFieldNotNull<O: Any>(val propertyA: KProperty1<O, Any?>, val propertyB: KProperty1<O, Any?>) : MultiPropertyConstraintRule<Any?>
data class  ObjectNullIfOtherFieldNotNull<O: Any>(val propertyA: KProperty1<O, Any?>, val propertyB: KProperty1<O, Any?>) : MultiPropertyConstraintRule<Any?>
object ObjectNull : ConstraintRule<Any?>

fun <F : Any?> FieldConstraint<out Any, F>.notNull() {
    constraintRules.add(ObjectNotNull)
}

fun <O: Any, F : Any?> FieldConstraint<O, F>.notNullIfOtherFieldIsNotNull(property: KProperty1<O, Any?>) {
    constraintRules.add(ObjectNotNullIfOtherFieldNotNull(this.property, property))
}

fun <F : Any?> FieldConstraint<out Any, F>.shouldBeNull() {
    constraintRules.add(ObjectNull)
}

fun <O: Any, F : Any?> FieldConstraint<O, F>.shouldBeNullIfOtherFieldIsNotNull(property: KProperty1<O, Any?>) {
    constraintRules.add(ObjectNullIfOtherFieldNotNull(this.property, property))
}
