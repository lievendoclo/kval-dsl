package be.sourcedbvba.kval.dsl

data class CharSequenceSize(val min: Int, val max: Int) : ConstraintRule<CharSequence>
object CharSequenceNotBlank : ConstraintRule<CharSequence>
object ValidEmailAddress: ConstraintRule<CharSequence>
object ValidCreditCardNumber : ConstraintRule<CharSequence>
object ValidURL : ConstraintRule<CharSequence>
data class CompliesWithRegex(val pattern: CharSequence) : ConstraintRule<CharSequence>

fun <C : CharSequence> FieldConstraint<out Any, C>.notBlank() {
    constraintRules.add(CharSequenceNotBlank)
}

fun <C : CharSequence> FieldConstraint<out Any, C>.size(min: Int = 0, max: Int = Int.MAX_VALUE) {
    constraintRules.add(CharSequenceSize(min, max))
}

fun <C : CharSequence> FieldConstraint<out Any, C>.emailAddress() {
    constraintRules.add(ValidEmailAddress)
}

fun <C : CharSequence> FieldConstraint<out Any, C>.urlAddress() {
    constraintRules.add(ValidURL)
}

fun <C : CharSequence> FieldConstraint<out Any, C>.creditCardNumber() {
    constraintRules.add(ValidCreditCardNumber)
}

fun <C : CharSequence> FieldConstraint<out Any, C>.regex(pattern: CharSequence) {
    constraintRules.add(CompliesWithRegex(pattern))
}
