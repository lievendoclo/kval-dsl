package be.sourcedbvba.kval.dsl

import java.math.BigDecimal

data class NaturalNumberMin<N : Number>(val value: N) : ConstraintRule<Number>
data class NaturalNumberMax<N : Number>(val value: N) : ConstraintRule<Number>
data class NaturalNumberRange<N : Number>(val min: N, val max: N) : ConstraintRule<Number>
object PositiveNumber : ConstraintRule<Number>
object NegativeNumber : ConstraintRule<Number>

data class DecimalNumberMin<N : Number>(val value: N) : ConstraintRule<Number>
data class DecimalNumberMax<N : Number>(val value: N) : ConstraintRule<Number>

fun <N: Number> FieldConstraint<out Any, N>.positive() {
    constraintRules.add(PositiveNumber)
}

fun <N: Number> FieldConstraint<out Any, N>.negative() {
    constraintRules.add(NegativeNumber)
}

fun FieldConstraint<out Any, Int>.min(value: Int) {
    constraintRules.add(NaturalNumberMin(value))
}

fun FieldConstraint<out Any, Int>.max(value: Int) {
    constraintRules.add(NaturalNumberMax(value))
}

fun FieldConstraint<out Any, Int>.range(min: Int, max: Int) {
    constraintRules.add(NaturalNumberRange(min, max))
}

fun FieldConstraint<out Any, Long>.min(value: Long) {
    constraintRules.add(NaturalNumberMin(value))
}

fun FieldConstraint<out Any, Long>.max(value: Long) {
    constraintRules.add(NaturalNumberMax(value))
}

fun FieldConstraint<out Any, Long>.range(min: Long, max: Long) {
    constraintRules.add(NaturalNumberRange(min, max))
}

fun FieldConstraint<out Any, Float>.min(value: Float) {
    constraintRules.add(DecimalNumberMin(value))
}

fun FieldConstraint<out Any, Float>.max(value: Float) {
    constraintRules.add(DecimalNumberMax(value))
}

fun FieldConstraint<out Any, Double>.min(value: Double) {
    constraintRules.add(DecimalNumberMin(value))
}

fun FieldConstraint<out Any, Double>.max(value: Double) {
    constraintRules.add(DecimalNumberMax(value))
}

fun FieldConstraint<out Any, BigDecimal>.min(value: BigDecimal) {
    constraintRules.add(DecimalNumberMin(value))
}

fun FieldConstraint<out Any, BigDecimal>.max(value: BigDecimal) {
    constraintRules.add(DecimalNumberMax(value))
}