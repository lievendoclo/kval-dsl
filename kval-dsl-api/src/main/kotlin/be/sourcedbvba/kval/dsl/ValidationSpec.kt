package be.sourcedbvba.kval.dsl

import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

data class ValidationSpec(val constraints: MutableList<Constraints<out Any>> = mutableListOf())

data class Constraints<T : Any>(val klass: KClass<T>, val fieldConstraints: MutableList<FieldConstraint<T, out Any?>> = mutableListOf())

data class FieldConstraint<T : Any, P: Any?>(val property: KProperty1<T, P>,
                                            val constraintRules: MutableList<ConstraintRule<in P>> = mutableListOf()) {}

interface ConstraintRule<T : Any?>

interface MultiPropertyConstraintRule<T : Any?> : ConstraintRule<T>

fun validationSpec(block: ValidationSpec.() -> Unit) : ValidationSpec {
    val validationSpec = ValidationSpec()
    block(validationSpec)
    return validationSpec
}

inline fun <reified T : Any> ValidationSpec.constraints(block: Constraints<T>.() -> Unit) {
    val constraints = Constraints(T::class)
    this.constraints.add(constraints)
    block(constraints)
}

fun <T : Any, P : Any?> Constraints<T>.field(property: KProperty1<T, P>, block: FieldConstraint<T, P>.() -> Unit) {
    val fieldConstraint = FieldConstraint(property)
    fieldConstraints.add(fieldConstraint)
    block(fieldConstraint)
}